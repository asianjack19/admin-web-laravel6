<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Item extends Model
{
    //
    protected $table = "item";
    protected $fillable = ["nama", "harga", "item_kategori"];

    public function ItemKategori()
    {
        return $this->belongsTo('App\Models\ItemKategori');
    }
}
