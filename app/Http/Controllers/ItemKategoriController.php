<?php

namespace App\Http\Controllers;

use App\ItemKategori;
use Illuminate\Http\Request;

class ItemKategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function index(){
        //$pertanyaan = DB::table('pertanyaan')->get();

        //eloquent
        $item_kategori=ItemKategori::all();
        return view('item_kategori.index', compact('item_kategori'));
    }

    function create(){

        return view('item_kategori.create');
    }

    function store(Request $request){
        $request->validate([
            'nama' => 'required|unique:item_kategori',
        ]);

        $item_kategori=ItemKategori::create(['nama' => $request["nama"]]);

        return redirect('/itemkategori');
    }

    function show($id){
        //$pertanyaan = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        $item_kategori = ItemKategori::find($id);

        return view('item_kategori.show', compact('item_kategori'));
    }

    function edit($id){
        $item_kategori = ItemKategori::find($id);
        return view('item_kategori.edit', compact('item_kategori'));
    }

    function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
        ]);
        //mass update
        ItemKategori::where('id', $id)
          ->update(['nama' =>  $request["nama"],
                    ]);


        // $query = DB::table('pertanyaan')
        //     ->where('id',$pertanyaan_id)
        //     ->update([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"],
        //     "tanggal_diperbaharui" => date("Y-m-d")
        // ]);
        return redirect('/itemkategori');
    }

    function destroy($id){
        //$query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        //deleteing an existing model on key

        ItemKategori::destroy($id);

        return redirect('/itemkategori');
    }

}
