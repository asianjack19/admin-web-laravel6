<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Transaksi;
use App\TransaksiDetail;
use DB;
use Yajra\DataTables\Facades\DataTables;

class TransaksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function createTransaksi()
    {
        $item = Item::orderBy('created_at', 'DESC')->get();
        return view('transaksi.create', compact('item'));
    }

    public function getItem(Request $request)
    {
        $id=$request["id"];
        $item = Item::findOrFail($id);
        return response()->json($item, 200);
    }


    public function storeTransaksi(Request $request)
    {
        //transaksi

        $transaksi=new Transaksi();
        $transaksi->id_user=1; //sementara satu, nanti login dipasang ini ganti auth id
        $transaksi->nama_pelanggan=$request['nama_pelanggan'];
        $transaksi->jumlah_total=$request['total_keranjang'];
        $transaksi->save();

        $id_trans=$transaksi->id; //untuk di insertkan di transaksi detail



        for ($i = 0; $i < count($request['id_item']); $i++)
        {
            $transaksi_detail=new TransaksiDetail();
            $transaksi_detail->id_item=$request['id_item'][$i];
            $transaksi_detail->transaksi_id=$id_trans;
            $transaksi_detail->qty=$request['qty'][$i];
            $transaksi_detail->harga=$request['harga_item'][$i];
            $transaksi_detail->subtotal=$request['subtotal'][$i];
            $transaksi_detail->save();
        }

        return redirect('transaksi');
        //detail_transaksi
    }

    public function index()
    {
        $transaksi = DB::table('transaksi')->latest()->get();
        //dd($transaksi);
        return view('transaksi.index', compact('transaksi'));
    }

    public function show($id)
    {
        $transaksi = Transaksi::find($id);
        //dd($transaksi);
        //$transaksi_detail = DB::table('transaksi_detail')->where('transaksi_id', $id)->get();

        $transaksi_detail=DB::table('transaksi_detail as td')
        ->select('td.id as id','i.nama as nama','td.harga as harga','td.qty as qty','td.subtotal as subtotal')
        ->join('item as i','i.id','=','td.id_item')
        ->where('transaksi_id', $id)
        ->get();
        //dd($transaksi_detail);
        $i=1;

        $transaksi_sum=DB::table('transaksi_detail')
        ->where('transaksi_id', $id)
        ->sum('subtotal');

        return view('transaksi.show', compact('transaksi','transaksi_detail','transaksi_sum','i'));

        //->with('transaksi_detail', $transaksi_detail);;
    }

    public function detail($id)
    {
        $transaksi = Transaksi::find($id);

        $transaksi_detail=DB::table('transaksi_detail as td')
        ->select('td.id as id','i.nama as nama','td.harga as harga','td.qty as qty','td.subtotal as subtotal')
        ->join('item as i','i.id','=','td.id_item')
        ->where('transaksi_id', $id)
        ->get();
        //dd($transaksi_detail);
        $i=1;

        $transaksi_sum=DB::table('transaksi_detail')
        ->where('transaksi_id', $id)
        ->sum('subtotal');
        
        //->get();
        //dd($transaksi_sum);

        //return Datatables::of($transaksi_detail)->make(true);

        
       return view('transaksi.cetak', compact('transaksi','transaksi_detail', 'transaksi_sum','i'));
        //$pdf = PDF::loadView('transaksi.cetak', compact('transaksi','transaksi_detail', 'transaksi_sum','i'));
        //return $pdf->download('transaksi.pdf');
    }

    public function cetak($id)
    {
        $transaksi = Transaksi::find($id);

        $transaksi_detail=DB::table('transaksi_detail as td')
        ->select('td.id as id','i.nama as nama','td.harga as harga','td.qty as qty','td.subtotal as subtotal')
        ->join('item as i','i.id','=','td.id_item')
        ->where('transaksi_id', $id)
        ->get();
        //dd($transaksi_detail);
        $i=1;

        $transaksi_sum=DB::table('transaksi_detail')
        ->where('transaksi_id', $id)
        ->sum('subtotal');
        
        //->get();
        //dd($transaksi_sum);

        return Datatables::of($transaksi_detail)->make(true);

        
       //return view('transaksi.cetak', compact('transaksi','transaksi_detail', 'transaksi_sum','i'));
        //$pdf = PDF::loadView('transaksi.cetak', compact('transaksi','transaksi_detail', 'transaksi_sum','i'));
        //return $pdf->download('transaksi.pdf');
    }

    public function destroy($id)
    {
        $query = DB::table('transaksi')->where('id', $id)->delete();
        return redirect('/transaksi/list');
    }
}
