<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = "transaksi";

    protected $fillable = ['id_user','nama_pelanggan','jumlah_total'];
}
