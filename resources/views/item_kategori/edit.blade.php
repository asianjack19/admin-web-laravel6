@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Ubah Item Kategori</h4>
                </div>
                <form action="{{route('itemkategori.update',['itemkategori'=>$item_kategori->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input type="text" class="form-control" name="nama" value="{{$item_kategori->nama}}" id="isi" placeholder="Masukkan Nama Item Kategori">
                        @error('title')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
