@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Ubah Item</h4>
                </div>
                <form action="{{route('item.update',['item'=>$item->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input type="text" class="form-control" name="nama" value="{{$item->nama}}" id="isi" placeholder="Masukkan Nama Item">
                        <label for="title">Harga</label>
                        <input type="text" class="form-control" name="harga" value="{{$item->harga}}" id="isi" placeholder="Masukkan Harga Item">
                        <label for="title">Kategori Item</label>
                        <select class="custom-select" id="item_kategori" name="item_kategori" id="item_kategori">
                             @foreach ($item_kategori as $value)
                             @if($value->id ==$item->kategori_id)
                                   <option value="{{ $value->id }}" selected>{{ $value->nama }}</option>
                             @else
                                    <option value="{{ $value->id }}">{{ $value->nama }}</option>
                             @endif
                             @endforeach
                        </select>
                        @error('title')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
