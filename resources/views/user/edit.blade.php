@extends('layouts.master')

@push('css')
<style type="text/css">
    .btn{
        margin-right: 2px;margin-left: 2px;
    }
 </style>
@endpush


@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">
                    <h4>Edit Data User</h4>
                </div>
                <form action="{{route('user.update', ['user'=>$user->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Nama User</label>
                        <input type="text" class="form-control" value="{{$user->name}}" name="name" id="name" placeholder="Masukkan Nama User Baru">
                        <label for="title">Email User</label>
                        <input type="text" class="form-control" value="{{$user->email}}" name="email" id="email" placeholder="Masukkan Email User Baru">

                        <label for="title">Password</label>
                        <input type="text" class="form-control" name="password" id="password" placeholder="Masukkan Password User">
                        <label for="title">Role User</label>
                        <select class="custom-select" id="role_id" name="role_id">
                            <option value=""> --Silahkan Pilih-- </option>
                            @foreach ($user_role as $value)
                                    @if($value->id ==$user->role_id)
                                    <option value="{{ $value->id }}" selected>{{ $value->nama }}</option>
                                     @else
                                            <option value="{{ $value->id }}">{{ $value->nama }}</option>
                                 @endif

                            @endforeach
                       </select>

                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

@endpush
