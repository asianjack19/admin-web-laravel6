<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

use App\Http\Controllers\ItemController;


Route::resource('itemkategori', 'ItemKategoriController');
Route::resource('userrole', 'UserRoleController');
Route::resource('item','ItemController');
Route::resource('user', 'UserController');
Route::resource('transaksi_list', 'TransaksiListController');

Route::get('/transaksi', 'TransaksiController@createTransaksi')->name('create_transaksi');

Route::post('/transaksi', 'TransaksiController@getItem')->name('get_item_transaksi');

Route::post('/transaksi/store', 'TransaksiController@storeTransaksi')->name('store_transaksi');

Route::get('/transaksi/list', 'TransaksiController@index');
Route::get('/transaksi/list/{id}', 'TransaksiController@show');
Route::get('/transaksi/list/{id}/edit', 'TransaksiController@edit');
Route::delete('/transaksi/list/{id}', 'TransaksiController@destroy');

Route::get('/transaksi/list/{id}/detail','TransaksiController@detail');
Route::get('/transaksi/list/{id}/cetak','TransaksiController@cetak');
Route::get('/dashboard', 'DashboardController@count');


//Route::get('/dashboard', function () {
//    return view('dashboard');

//});
