<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemKategoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_kategori', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('id');
            $table->string('nama',50)->unique();
            $table->integer('is_deleted')->default(0);
            $table->timestamps();
        });

        DB::table('item_kategori')->insert(
            array(
                'nama' => 'Makanan',
                'created_at' => now(),
                'updated_at' => now(),
            )
        );
        DB::table('item_kategori')->insert(
            array(
                'nama' => 'Minuman',
                'created_at' => now(),
                'updated_at' => now(),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_kategori');
    }
}
